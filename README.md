# gitlab-backup
GitLab Backup is a project used to export and import full GitLab instances using GitLab API.


## Prerequisite

* Configured Gitlab API Token, https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html


## Limitations

* Gitlab API limitations, https://docs.gitlab.com/ee/user/project/settings/import_export.html#rate-limits


## Install

Install via pip:
```sh
$ pip install gitlab-backup
```

or

```sh
$ pip install git+https://gitlab.com/aylook-public/gitlab-backup
```

## Configuration
System uses yaml file as configuration.

Example yaml:
```yaml
gitlab:                                                   - gitlab configuration
  access:
    gitlab_url: "https://gitlab.com"                      - Gitlab url, official or your instance
    token: "MY_PERSONAL_SECRET_TOKEN"                     - personal access token
    ssl_verify: True                                      - verify SSL certificate
  membership: True                                        - export all projects use False
  rate_limits:                                            - rate limits 
    export: 60                                            - wait time until schedule next export in seconds
    download: 300                                         - wait time until download next export file in seconds
  max_tries_number: 240                                   - maximum number of tries for creating export
  projects:                                               - list of projects to export
    - aylook-public/gitlab-backup

backup:                                                   - backup configuration
  project_dirs: True                                      - store projects in separate directories
  destination: "/data/backup"                             - base backup dir
  backup_name: "gitlab-com-{PROJECT_NAME}-{TIME}.tar.gz"  - backup file template
  backup_time_format: "%Y%m%d"                            - TIME tamplate, use whatever compatible with python datetime - date.strftime()
  clone_failed: True                                      - clones projects that failed to export
  s3_config:                                              - S3 config
    region_name: "eu-south-1"                             - region name
    access_key: "*********"                               - access key value
    secret_access_key: "*********"                        - secret key value
    endpoint_url: "https://*****.**"                      - endpoint URL
    bucket_name: "my_bucket"                              - name of bucket
    prefix: "my_directory"                                - name of directory
    expiration: 30                                        - expiration for files stored in bucket
```

## Export

Create configuration file and run:
```sh
$ ./gitlab-cli export -c /path/to/config.yaml
```
for debug mode add `-d`


## Import
TODO
