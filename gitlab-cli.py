#!/usr/bin/env python

import asyncio
import logging
from typing import Callable
from argparse import ArgumentParser


def parent_parser() -> ArgumentParser:
    parser = ArgumentParser(add_help=False)
    parser.add_argument('--config',
                        '-c',
                        type=str,
                        default='config.yaml',
                        help='Config file')

    parser.add_argument('--debug',
                        '-d',
                        default=False,
                        action='store_const',
                        const=True,
                        help='Debug mode')

    return parser


def execute(func: Callable, *args, **kwargs):

    loop = asyncio.get_event_loop()

    try:
        loop.run_until_complete(func(*args, **kwargs))
    except KeyboardInterrupt:
        pass
    except Exception as e:
        logging.error(f'Error: {e!r}')
    finally:
        loop.run_until_complete(loop.shutdown_asyncgens())
        loop.close()


if __name__ == '__main__':

    # setup parser
    parser = ArgumentParser(
        description='''
        GitLab Backup is a project used to export and import full GitLab
        instances using GitLab API.
        ''',
        epilog='Created by Aylook Team')
    parent = parent_parser()

    # setup subparsers
    subparsers = parser.add_subparsers(dest='action')
    subparsers.add_parser('export',
                          parents=[parent],
                          help='Export GitLab projects')
    subparsers.add_parser('import',
                          parents=[parent],
                          help='Import GitLab projects')

    args = parser.parse_args()
    debug = args.debug if args.action else False
    logging.basicConfig(
        level=logging.DEBUG if debug else logging.WARNING,
        format='%(asctime)s - [%(levelname)s] - %(message)s'
    )
    if args.action == 'export':
        from gitlab import export
        logging.debug('Starting export ...')
        execute(export, config_path=args.config)
    elif args.action == 'import':
        logging.debug('Starting import ...')
        logging.info(args)
    else:
        parser.print_help()
