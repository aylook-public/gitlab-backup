import os
import re
import boto3
import asyncio
import logging
from datetime import date
from typing import List, Dict, Optional
from botocore.exceptions import ClientError
from mypy_boto3_s3.client import S3Client

from .api import Api
from .config import Config
from .project import Project, ProjectState


def get_s3_client(s3config: Dict) -> Optional[S3Client]:

    try:
        region_name = s3config['region_name']
        access_key = s3config['access_key']
        secret_access_key = s3config['secret_access_key']
        endpoint_url = s3config['endpoint_url']
        bucket_name = s3config['bucket_name']
        prefix = s3config['prefix']
        expiration = int(s3config['expiration'])
    except KeyError as e:
        logging.error('Error: %r' % e)
        return None

    client = boto3.client('s3',
                          region_name=region_name,
                          endpoint_url=endpoint_url,
                          aws_access_key_id=access_key,
                          aws_secret_access_key=secret_access_key)

    try:
        resp = client.get_bucket_lifecycle_configuration(Bucket=bucket_name)
    except ClientError:
        logging.debug('No bucket lifecycle configuration')
    else:
        for rule in resp['Rules']:
            if rule['ID'] == 'ExpirationBackupRule':
                return client

    try:
        client.put_bucket_lifecycle_configuration(
            Bucket='aylook-s3',
            LifecycleConfiguration={
                'Rules': [{
                    'ID': 'ExpirationBackupRule',
                    'Expiration': {'Days': expiration},
                    'Prefix': prefix,
                    'Status': 'Enabled'}]
            })
    except Exception as e:
        logging.error('Error: %r' % e)
        return None

    return client


async def _export_projects_task(config: Config,
                                api: Api,
                                projects: List[Project]) -> None:
    '''
    Task for schedule projects export
    '''
    tasks = []
    for project in projects:
        tasks.append(asyncio.create_task(
            api.export(project,
                       max_tries_number=config.gitlab['max_tries_number'])))
        await asyncio.sleep(config.gitlab['rate_limits']['export'])

    while [t for t in tasks if not t.done() or t.cancelled()]:
        await asyncio.sleep(10)


async def _download_projects_task(config: Config,
                                  api: Api,
                                  projects: List[Project]) -> None:
    '''
    Task for downloading ready export files for projects
    '''
    def _get_not_completed_projects(projects: List[Project]):
        return [project.name for project in projects
                if project.state not in [ProjectState.ERROR,
                                         ProjectState.DOWNLOADED]]
    while True:
        for project in projects:
            if project.state == ProjectState.READY:
                logging.debug(f'Downloading project: {project.name}')
                await api.download(project)
                not_completed_projects = _get_not_completed_projects(projects)
                logging.debug(f'Pending projects for download: {not_completed_projects}')
                if not_completed_projects:
                    await asyncio.sleep(config.gitlab['rate_limits']['download'])

        not_completed_projects = _get_not_completed_projects(projects)
        if not not_completed_projects:
            return

        await asyncio.sleep(15)


async def _clone_projects_task(config: Config, projects: List[Project]) -> None:
    '''
    Task for cloning gitlab repository
    '''
    for project in projects:
        if project.state != ProjectState.ERROR:
            continue

        project.clone(config.gitlab['access']['token'])


async def export(config_path):
    '''
    Get projects, schedule export and download export file
    '''
    config = Config(config_path)
    api = Api.from_config(config)
    if not api:
        logging.debug('Can not create api. Check config file.')
        return

    all_projects = await api.project_list()

    projects = []
    for project_pattern in config.gitlab['projects']:
        for project in all_projects:
            if re.match(project_pattern, project['path_with_namespace']):
                projects.append(Project.from_dict(project))

    # Prepare actual date
    d = date.today()

    # Download project to our destination
    for project in projects:
        destination = config.backup['destination']
        if config.backup['project_dirs']:
            destination += '/' + project.path_with_namespace

        # Create directories
        if not os.path.isdir(destination):
            try:
                os.makedirs(destination)
            except Exception:
                logging.debug('Unable to create directories %s' % (destination))
                return

        # File template from config
        file_tmpl = config.backup['backup_name']

        project.dest_file = destination + '/' + file_tmpl.format(
            PROJECT_NAME=project.path_with_namespace.replace("/", "-"),
            TIME=d.strftime(config.backup['backup_time_format']))

        # Is project already downloaded
        if os.path.isfile(project.dest_file):
            logging.debug(f'File {project.dest_file} already exists. Skipping')
            projects.remove(project)
            continue

    if not projects:
        logging.debug('No projects available for download')
        return

    futures = []
    futures.append(_export_projects_task(config, api, projects))
    futures.append(_download_projects_task(config, api, projects))
    await asyncio.gather(*futures)

    # clone failed projects
    if config.backup.get('clone_failed', False):
        await _clone_projects_task(config, projects)

    # upload to S3
    if s3config := config.backup.get('s3_config', None):
        if client := get_s3_client(s3config):
            for project in projects:
                if project.state not in [ProjectState.DOWNLOADED,
                                         ProjectState.CLONED]:
                    continue
                try:
                    client.upload_file(
                        project.dest_file,
                        s3config['bucket_name'],
                        f'{s3config["prefix"]}/{date.today().isoformat()}/{os.path.basename(project.dest_file)}')
                except Exception:
                    logging.error(f'Error uploading file to bucket. Project: {project.name}')
                else:
                    if os.path.exists(project.dest_file):
                        os.remove(project.dest_file)

    for project in projects:
        logging.info(project)
