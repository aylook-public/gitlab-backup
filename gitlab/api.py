from __future__ import annotations
import asyncio
import aiohttp
import logging
from typing import Optional, Dict, List
from urllib.parse import urlencode
from .config import Config
from .project import Project, ProjectState


CHUNK_SIZE = 1024 * 1024 * 64


class Api:

    _instance = None

    def __new__(cls, *args, **kwargs):
        if not isinstance(cls._instance, cls):
            cls._instance = object.__new__(cls)
        return cls._instance

    def __init__(self, gitlab_url: str, token: str, ssl: bool = True):
        self.headers: Dict[str, str] = {'PRIVATE-TOKEN': token}
        self.api_url: str = gitlab_url + '/api/v4'
        self.ssl: bool = ssl

    async def __get(self, endpoint: str):
        async with aiohttp.ClientSession() as session:
            async with session.get(self.api_url + endpoint,
                                   headers=self.headers,
                                   ssl=self.ssl) as resp:
                data = await resp.json()
                return resp.status, data

    async def __post(self, endpoint: str):
        async with aiohttp.ClientSession() as session:
            async with session.post(self.api_url + endpoint,
                                    headers=self.headers,
                                    ssl=self.ssl) as resp:
                data = await resp.json()
                return resp.status, data

    async def __download(self, endpoint: str, project: Project):

        if not project.dest_file:
            logging.debug(f'No destination folder. Project: {project.name}')
            raise Exception('No destination folder')

        downloaded = 0
        notify_range = [i for i in range(20, 101, 20)]

        async with aiohttp.ClientSession() as session:
            async with session.get(self.api_url + endpoint,
                                   headers=self.headers,
                                   ssl=self.ssl) as resp:
                size = int(resp.headers.get('content-length', 0))
                if size == 0:
                    raise Exception('Download file is not ready.')

                logging.debug(f'Download file size: {size}. Project: {project.name}')

                with open(project.dest_file, 'wb') as f:
                    async for chunk in resp.content.iter_chunked(CHUNK_SIZE):
                        f.write(chunk)
                        downloaded += len(chunk)
                        progress = round(downloaded / size * 100)
                        if progress in notify_range:
                            logging.debug(f'Downloaded: {progress}%. Project: {project.name}')
                            notify_range.pop(notify_range.index(progress))

    async def project_list(self, membership: bool = True) -> Optional[List[Dict]]:
        '''
        List projects
        '''
        endpoint = '/projects?' + urlencode({'simple': True,
                                             'membership': membership,
                                             'per_page': 50})
        page = 1
        ret = []
        while True:
            try:
                status, data = await self.__get(endpoint + f"&page={page}")
            except Exception as e:
                logging.error(f'API Error: {str(e)}')
                return []
            else:
                if status == 200:
                    if data:
                        ret.extend(data)
                        page += 1
                        continue
                    break
                else:
                    logging.error(f'API returned {status}')
                    return []

        return ret

    async def export(self, project: Project, max_tries_number: int,
                     timeout: int = 15) -> None:
        '''
        Schedule export
        '''
        # schedule a backup
        project.state = ProjectState.EXPORTING
        endpoint = f'/projects/{project.id}/export'
        for i in range(3):
            try:
                status, data = await self.__post(endpoint)
            except Exception as e:
                logging.error(f'API Error: {str(e)}')
            else:
                if status == 202:
                    logging.debug(f'Export is scheduled for download. Project: {project.name}')
                    project.state = ProjectState.EXPORTED
                    break

                if i == 2:
                    logging.debug(f'Can not schedule download. Project: {project.name}')
                    project.state = ProjectState.ERROR
                    return

            await asyncio.sleep(timeout)

        # check is backup finished / ready for download
        prev_export_status = ''
        max_tries = max_tries_number
        while max_tries != 0:
            # Decrement tries
            max_tries -= 1

            try:
                status, data = await self.__get(endpoint)
            except Exception as e:
                logging.error(f'API Error: {str(e)}')
            else:
                if status == 200:
                    if export_status := data.get('export_status'):
                        if export_status == 'finished':
                            logging.debug(f'Export is ready for download. Project: {project.name}')
                            project.state = ProjectState.READY
                            return
                        elif export_status != prev_export_status:
                            logging.debug(f'Export is not yet ready for download. Status: {export_status}. Project: {project.name}')
                        prev_export_status = export_status
                else:
                    logging.debug(f'Something is wrong with API. Project: {project.name}')
                    project.state = ProjectState.ERROR
                    break

            await asyncio.sleep(timeout)

        project.state = ProjectState.ERROR

    async def download(self, project: Project) -> None:
        '''
        Download export file
        '''
        project.state = ProjectState.DOWNLOADING
        endpoint = f'/projects/{project.id}/export/download'

        try:
            await self.__download(endpoint, project)
        except Exception as e:
            logging.error(f'Download error: {str(e)}')
            project.state = ProjectState.ERROR
        else:
            logging.debug(f'Export is downloaded. Project: {project.name}')
            project.state = ProjectState.DOWNLOADED

    @classmethod
    def from_config(cls, config: Config) -> Optional[Api]:
        '''
        Create API from config
        '''
        try:
            gitlab_url = config.gitlab['access']['gitlab_url']
            token = config.gitlab['access']['token']
            ssl_verify = config.gitlab['access']['ssl_verify']
        except Exception:
            return None
        else:
            return Api(gitlab_url, token, ssl_verify)
