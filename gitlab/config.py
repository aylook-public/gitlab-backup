from typing import Dict, Any
from yaml import load, FullLoader


class Config:
    '''
    Holds YAML file configuration
    '''
    def __init__(self, config_file_path: str) -> None:
        self.config = Config.load(config_file_path)

    def __getattr__(self, attr: str) -> Any:
        return self.config[attr]

    @classmethod
    def load(cls, config_file_path: str) -> Dict[str, Any]:
        with open(config_file_path, 'r') as f:
            return load(f.read(), Loader=FullLoader)
