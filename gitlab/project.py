from __future__ import annotations
import logging
from git import Repo
from tempfile import TemporaryDirectory
from typing import Dict, Optional
from enum import Enum


class ProjectState(Enum):
    PENDING = 0
    EXPORTING = 1
    EXPORTED = 2
    IMPORTING = 3
    IMPORTED = 4
    READY = 5
    DOWNLOADING = 6
    DOWNLOADED = 7
    CLONED = 8
    COMPLETED = 10
    ERROR = 11


class Project:
    ''' '''
    def __init__(self, id: int, name: str, path: str, path_with_namespace: str,
                 http_url_to_repo: str, web_url: str, namespace: Dict) -> None:
        self.id: int = id
        self.name: str = name
        self.path: str = path
        self.path_with_namespace: str = path_with_namespace
        self.http_url_to_repo: str = http_url_to_repo
        self.web_url: str = web_url
        self.namespace: Dict = namespace
        self.state: ProjectState = ProjectState.PENDING
        self.dest_file: Optional[str] = None

    def get_clone_http_url(self, personal_access_token: str) -> str:
        return f'https://oauth2:{personal_access_token}@gitlab.com/{self.path_with_namespace}.git'

    def clone(self, personal_access_token) -> None:
        if not self.dest_file:
            return
        try:
            with TemporaryDirectory() as tmpdirname:
                repo = Repo.clone_from(
                    self.get_clone_http_url(personal_access_token),
                    tmpdirname)
                with open(self.dest_file, 'wb') as fp:
                    repo.archive(fp)
        except Exception as e:
            logging.debug(f'Can not clone. Project: {self.name}. Error: {e!r}')
        else:
            logging.debug(f'Project is cloned. Project: {self.name}')
            self.state = ProjectState.CLONED

    @classmethod
    def from_dict(cls, entry: Dict) -> Project:
        return Project(
            id=entry['id'],
            name=entry['name'],
            path=entry['path'],
            path_with_namespace=entry['path_with_namespace'],
            http_url_to_repo=entry['http_url_to_repo'],
            web_url=entry['web_url'],
            namespace=entry['namespace'],
        )

    def __repr__(self):
        return f'<{self.__class__.__name__} name: {self.name}, state: {self.state.name}>'
