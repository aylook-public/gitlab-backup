import re
import sys
import pathlib
from setuptools import setup, find_packages


if sys.version_info < (3, 8):
    raise RuntimeError('gitlab-backup requires Python 3.8+')

here = pathlib.Path(__file__).parent

try:
    txt = (here / 'gitlab' / '__init__.py').read_text('utf-8')
    version = re.findall(r'^__version__ = \'([^\']+)\'\r?$', txt, re.M)[0]
    print(f'version: {version!r}')
except IndexError:
    raise RuntimeError('Unable to determine version.')


with open('README.md', 'r') as f:
    long_description = f.read()

with open('requirements.txt', 'r') as f:
    install_requires = f.read().split()


setup(
    name='gitlab-backup',
    version=version,
    description=('GitLab Backup is a project used to export and import '
                 'full GitLab instances using GitLab API'),
    long_description=long_description,
    long_description_content_type='text/markdown',
    include_package_data=True,
    author='Antek-AAD',
    author_email='dalibor.stankovic@antek-aad.com',
    url='https://gitlab.com/aylook-public/gitlab-backup',
    download_url='',
    packages=find_packages(),
    install_requires=install_requires,
    scripts=['gitlab-cli.py'],
    keywords=['gitlab-backup', 'gitlab', 'export', 'import', 'backup'],
    classifiers=[
        'License :: OSI Approved :: BSD License',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
    ]
)
